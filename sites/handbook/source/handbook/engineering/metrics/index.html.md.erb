---
layout: handbook-page-toc
title: "Engineering Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Centralized Engineering Dashboards

Our centralized engineering dashboards provide a set of common metrics that capture the overall health of the entire R&D Product/Engineering structure, with drill downs into every stage and group.

This work is the product of the team working in our unified [engineering metrics task process](/handbook/engineering/quality#engineering-metrics-task-process).
The inception of this initiative can be see in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3580).

### Dashboard Navigation

* [Dev Section](/handbook/engineering/development/performance-indicators/dev/)
  * [Create Stage](/handbook/engineering/development/performance-indicators/dev/create)
  * [Plan Stage](/handbook/engineering/development/performance-indicators/dev/plan)
  * [Manage Stage](/handbook/engineering/development/performance-indicators/dev/manage)
* [Ops Section](/handbook/engineering/development/performance-indicators/ops/)
  * [Verify Stage](/handbook/engineering/development/performance-indicators/ops/verify)
  * [Package Stage](/handbook/engineering/development/performance-indicators/ops/package)
  * [Configure Stage](/handbook/engineering/development/performance-indicators/ops/configure)
  * [Monitor Stage](/handbook/engineering/development/performance-indicators/ops/monitor)
  * [Release Stage](/handbook/engineering/development/performance-indicators/ops/release)
* Sec Section
  * [Secure Stage](/handbook/engineering/development/performance-indicators/secure/)
  * [Protect Stage](/handbook/engineering/development/performance-indicators/protect/)
* [Growth Section](/handbook/engineering/development/performance-indicators/growth/)
* [Enablement Section](/handbook/engineering/development/performance-indicators/enablement/)

### Metrics list

#### Development indicators

* MRs vs Issues
* MR Rate, this rolls up to a KPI for the Development department
* Review to Merge time
* Product MRs by type
* Narrow MRs vs Wider MRs
* Feature flags older than 2 months
* MRs authored by team members vs community contribution MRs

#### Infrastructure indicators

* Corrective Actions past SLO
* Product Incidents

#### Quality indicators

* S1 Mean Time to Close, this rolls up to a KPI for the Quality department
* Average Age of unresolved S1 bugs
* S2 Mean Time to Close, this rolls up to a KPI for the Quality department
* Average Age of unresolved S2 bugs

#### UX indicators

* UX Debt open/close
* Average days to close UX Debt

#### Security indicators

* Average Age of currently open bug vulnerabilities, this rolls up to a KPI for the Security department
* MTTM of vulnerabilities by month

### Helpful Pointers

* Review the chart regularly and take notes of your group, stage or section's trends.
* Take note of anything that might be impacting the team's capacity such as holidays or increased PTO.
* Take note of your team's focus on community contribution as an example. If the team is able to consistently merge MRs in this categories, celebrate it.
* If you see a large amount `undefined`, spend some time to review your team's issues and MRs and add labels so we can get a more accurate classification.

## Merge request rate

Merge Request (MR) Rate is a measure of productivity and efficiency. The numerator is a collection of merge requests to a set of projects. The denominator is a collection of people. Both are tracked over time (usually monthly).

[Identity]` is collection of people (the denominator) and is usually a Division, Department, Sub-Department, or Team name from  our [Organizational Structure](/company/team/structure/#organizational-structure)

MR Rate prefixes have been removed in favor of focusing on what was previously known as Narrow MR Rate. Narrow MR Rates are now referred to as MR Rate in Engineering performance indicators.

### Examples

A 5 member team in the past month has merged 200 MRs authored by team members, merged 100 MRs authored by other GitLab team members, and 50 MRs authored by people from the wider-community.

The Team MR Rate would be 40 (200 / 5)

## Data Classification

We use the following type labels to classify our Issues and Merge Requests.

1. `~"Community contribution"`: A community contribution label takes precedence over other labels. Therefore, while the work may introduce a new feature or resolve a bug, we prioritize this label over others due to the importance of this particular category.
You may apply a second type label such as `~"bug"` or `~"feature"` to indicate the type of issue or merge request.
1. `~"security"`: Security-related MRs.
1. `~"bug"`: Defects in shipped code. Read more about [features vs bugs](/handbook/product/product-processes/#issues).
1. `~"feature"`: Any MR that contains work to support the implementation of a feature and/or results in an improvement in the user experience. Read more about [features vs bugs](/handbook/product/product-processes/#issues).
  - `~"feature::addition"`: Refers to the first MVC that gives GitLab users a foundation of new capabilities that were previously unavailable. For example, these issues together helped create the first MVC for our Reviewer feature: [Create a Reviewers sidebar widget](https://gitlab.com/gitlab-org/gitlab/-/issues/237921), [Show which reviewers have commented on an MR](https://gitlab.com/gitlab-org/gitlab/-/issues/10294), [Add reviewers to MR form](https://gitlab.com/gitlab-org/gitlab/-/issues/216054), [Increase MR counter on navbar when user is designated as reviewer](https://gitlab.com/gitlab-org/gitlab/-/issues/255102)
  - `~"feature::enhancement"`: Refers to GitLab user-facing improvements that refine the initial MVC to make it more useful and usable. For example, these issues enhance the existing Reviewer feature: [Show MRs where user is designated as a Reviewer on the MR list page](https://gitlab.com/gitlab-org/gitlab/-/issues/237922), [Display which approval rules match a given reviewer](https://gitlab.com/gitlab-org/gitlab/-/issues/233736), [Add Reviewers quick action](https://gitlab.com/gitlab-org/gitlab/-/issues/241244)
  - `~"feature::maintenance"`: Refers to refinements to an existing feature that are not GitLab user-facing and not related to `~bug` resolution. This could include `~"technical debt"` and industry-standard updates such as work towards Rails upgrade. For example: [Updating software versions in our tech stack](https://gitlab.com/gitlab-org/ci-cd/codequality/-/issues/22), [Recalculating UUIDs for vulnerabilities using UUIDv5](https://gitlab.com/gitlab-org/gitlab/-/issues/212322)
1. `~"tooling"`: MRs related to engineering tooling.
  - `~"tooling::pipelines"`: MRs related to pipelines configuration.
  - `~"tooling::workflow"`: MRs related to improvements of the engineering workflow and release tooling like Danger, RuboCop, linters, etc.
1. `~"documentation"`: For documentation-only MRs, use `~"documentation"` only unless the work is attributable to code changes for a feature or bug, and in that case, use `~"feature"` or `~"bug"`, even if the doc change is being made late for a feature/bug from a previous milestone.

If these labels are missing, it will be tracked in the `undefined` bucket instead.
The Engineering Manager for each team is ultimately responsible for ensuring that these labels are set correctly.

### Deprecated types

`~"backstage"` was intended to be changes that were done to keep product development running smoothly. Over time, `~"backstage"` was also being used for pre-feature work and has become unclear and confusing. `~"backstage"` was deprecated as part of <https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/488>.

### Suggestions for previous common ~"backstage" examples

This guidance may be helpful if you are wondering the go-forward type label based on your use case for applying `~"backstage"`.

- `~"feature::maintenance"` for industry standard and refactoring changes such as:
  - `~"technical debt"`
  -  non-`~"security"` `~"dependency update"`
  - `~"railsx.y"`
  - `~"Architecture Decision"`
- `~"feature::maintenance"` for addition or updates to specs for existing GitLab features
- `~"feature::addition"` for all changes related to the release of a new feature
- `~"tooling::workflow"` for changes to engineering workflows such as:
  - `~"Danger bot"`
  - `~"static analysis"`
  - release tooling
  - Docs tooling changes
- `~"tooling::pipelines"` for changes to project pipeline configurations

`~backstage` will be removed with <https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/483>.


### Stage and Group labels

In the spirit of "Everyone can Contribute" it's natural that members in a group will contribute to another group.
Our guideline aims to cover for the 20/80 (default accounting method). By default the MR from an author should belong to their `group::xxx` and direct parent `devops::xxx`
Optimizing for all edge cases will lead to complexity since there will always be edge cases.

We allow flexibility where the parent `devops::xxx` and child `group::xxx` label may not match. For example:
* In the case where labelling was corrected by a human.
* When working on shared `frontend`, `backend` components or `backstage` work that spans multiple groups.

If a contribution happens across groups, we leave it to the discretion of the engineering and product manager to change the `group::xxx` label to reflect which group worked on it.
They can also decide if they want to move over the `devops::xxx` as well or keep it to reflect the product area.
The [triage bot](https://gitlab.com/gitlab-org/quality/triage-ops/) automatic labelling we will not override existing labels.

## Projects that are part of the product

In the MR Rate and Volume of MR calculations, we consider MRs from projects that contributes to the overall product efforts.

The current list of projects are identified in the [`gitlab-data/analytics`](https://gitlab.com/gitlab-data/analytics) project for the following system databases:

| System Database | File |
|-----------------|------|
| GitLab.com      | [`projects_part_of_product.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv) |
| ops.gitlab.net  | [`projects_part_of_product_ops.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product_ops.csv)


### Updating the list of projects

The guidelines for inclusion in the `is_part_of_product` lists are:

- Included with the product as apart of a GitLab Omnibus or Cloud Native installation
- Support product development efforts
- Support the delivery and release process to GitLab SaaS

Follow these steps to request a new project to be tracked:

1. Create a merge request to the GitLab.com or ops.gitlab.net project list from above.
1. Assign the merge request to the [Manager of Engineering Productivity team](https://gitlab.com/kwiebers)
1. The Manager of the Engineering Productivity team will work with the [Development Operations Analyst](https://gitlab.com/lmai1) to determine the changes to MR Rate metrics and provide validation for the projects
1. The [VP of Development](https://gitlab.com/clefelhocz1) is the DRI to approve the list of projects. Upon approval the merge request author should ask in #data for assistance by a member of the Data Engineering team to merge.

There is no need to remove archived projects from the `is_part_of_product` list. Removal of projects will remove historical merge requests from metrics and reduce Merge Request rates.

Please reach out to a member of the [Engineering Productivity team](/handbook/engineering/quality/engineering-productivity-team/) if more assistance is needed
