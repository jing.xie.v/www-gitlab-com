---
layout: handbook-page-toc
title: Jamstack Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Jamstack Single-Engineer Group

The Jamstack SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

This group aims to provide an alternative to existing tools to help Front End developers build external facing hosted pages using the Jamstack architecture.
