---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@engwan](https://gitlab.com/engwan) | 1 | 680 |
| [@cablett](https://gitlab.com/cablett) | 2 | 600 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 4 | 500 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 400 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 8 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 9 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 10 | 250 |
| [@serenafang](https://gitlab.com/serenafang) | 11 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 15 | 200 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 18 | 140 |
| [@twk3](https://gitlab.com/twk3) | 19 | 130 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 21 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 100 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 30 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 31 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 32 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 33 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 34 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@ck3g](https://gitlab.com/ck3g) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@engwan](https://gitlab.com/engwan) | 1 | 680 |
| [@cablett](https://gitlab.com/cablett) | 2 | 600 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 4 | 500 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 400 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 8 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 9 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 10 | 250 |
| [@serenafang](https://gitlab.com/serenafang) | 11 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 15 | 200 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 18 | 140 |
| [@twk3](https://gitlab.com/twk3) | 19 | 130 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 21 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 100 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 30 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 31 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 32 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 33 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 34 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@ck3g](https://gitlab.com/ck3g) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


