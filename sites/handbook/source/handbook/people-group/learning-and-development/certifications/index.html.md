---
layout: handbook-page-toc
title: Certifications
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the certifications page! Here you will find links to our current certifications.

## Certifications 

Certifications at GitLab show how competent an individual team member is in applying knowledge and skills in the workplace and completion of training material. Certifications are summative in nature in that they measure the culmination of learning's and are all-encompassing with a formative assessment and learning path completion. GitLab has also begun to issue [external certifications](https://about.gitlab.com/services/education/gitlab-basics/) for GitLab users to demonstrate their skills and knowledge using GitLab solutions.

## Why Certification? 

Certification programs allow GitLab to validate skills, knowledge, and behavior to recognize individual mastery that motivate learners to grow skills and professional performance. 

### Current Certifications 

Team members can use [GitLab Learn](https://gitlab.edcast.com/) to complete many of the certifications we have to offer. 

* [GitLab 101](/handbook/people-group/learning-and-development/certifications/gitlab-101/)
* [GitLab 201](/handbook/people-group/learning-and-development/certifications/gitlab-201/)
* [Remote Work Foundations](/company/culture/all-remote/remote-certification/#remote-work-foundation-certification-criteria)
* [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management) - A certification featured on Coursera by GitLab 
* [Manager Challenge Program](/handbook/people-group/learning-and-development/manager-challenge/)
* [Diversity, Inclusion, and Belonging Certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05)

## Knowledge Assessments 

Knowledge Assessments at GitLab test knowledge of the specific content covered in training. Knowledge Assessments are formative learning that measures how an individual is performing against the training content. Knowledge Assessments are featured following the completion of a training event are typically five to ten questions in length. In addition, they can be one of many requirements to attain a certification. 

When you complete and pass a knowledge assessment, you may receive a certificate via email. A certificate is not equal to a certification on a topic. You can find some of our knowledge assessments on our [values](/handbook/communication/#gitlab-communication-knowledge-assessment), [communication](/handbook/total-rewards/compensation/#knowledge-assessment), [competencies](/handbook/competencies/#list), [inclusion](/company/culture/inclusion/being-inclusive/#inclusion-knowledge-assessment), and [total rewards](/total-rewards/benefits/#knowledge-assessment) handbook pages. 

## Sharing your Certificate 

### LinkedIn 

The video below shows you how to add your GitLab Certificate to your LinkedIn Profile. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_u9DImCd2OM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### LinkedIn with a PDF

If you received a PDF of your certificate and you have a Google account, you can follow these instructions to upload your certificate to your LinkedIn Profile. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NiyTxh-5ljM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Missed Certificate 

If you took one of our certifications or knowledge assessments and did not receive an email with a certificate attached please email our team learning@ and request the missing certificate. Please ensure to include which certificate/knowledge assessment it is as we have multiple. 

**Learning & Development Team Member process for sending out missing certificates**

1. When an email comes in requesting to be sent a missed certificate, search for the email address of certificate requester in the "pivot table 1" or "passing_results" or "pass_cert" or "pass_all" tab for the sheet that aligns with the certification or knowledge assessment. These sheets can be found in the [certifications & knowledge assessments folder](https://drive.google.com/drive/search?q=title:%22Certifications/Knowledge%20Assessments%22). 
   *  Note: The Remote Foundations Certification has 10 knowledge assessments to complete for certification and the GitLab 101 Tool Certification has 3 kowledge assessments to complete for certification. 
1. If they have completed all knowledge assessments, their email address will be seen in the "pivot table 1" tab.  If their email address is not found in the "pivot table 1" tab, see notes below for further steps.
1. To create their certificate, go to the relevant certification folder in the L&D google drive.  Make a copy of the certificate template and edit name and date accordingly.  Date should be in ISO format, YYYY-MM-DD.
1. Download the certificate as a PDF document.
1. When responding to the email, ensure to "reply all" so the learning@ email is included so the team knows the email has been handled. 
   * If not all necessary knowledge assessments have been completed, let them know which ones remain for them to receive their certificate. 
   * For email templates for different scenarios, check out our [Certification Request Responses doc](https://drive.google.com/drive/search?q=title:%22Certificate%20Request%20Responses%22). 

**Notes for the Remote Foundations Certification**
* If they have not completed one or multiple of the knowledge assessments, it will show up with a “0” in the column that is incomplete in the "pass_all" tab. 
* If the email doesn’t show up in the "pass_all" tab, check the tabs for each KA. If they don’t complete the Handbook First documentation knowledge assessment, they won’t show up in the “pass_all” tab at all (even if they have completed the other 9 KAs). The Handbook First Documentation knowledge assessment is the anchor for the "pass_all" tab. 

## How to Create a Knowledge Assessment 

### Step 1: Google Forms 

1. Go to the Shared L&D folder and go into the “Certifications/Knowledge Assessments” sub-folder. 
1. Select the “+ New” button on the top left and select “More” and then select “Google Forms” 
1. Create a google form with quiz questions for the topic. The quiz should have between 5-8 questions. 
1. In the Info section at the top: This knowledge assessment quiz is based on information found on our {name of page} page (including the video): {insert link to page}. To complete the {name} Knowledge Assessment at GitLab, you will need to read through the content on the page linked above and watch the video on the page (if you did not attend the live session) as well as complete this quiz and earn at least an 80%.
1. Settings → 
    1. General → Uncheck the “Restrict to users in GitLab and its trusted organizations” box. This ensures that internal team members as well as external people can complete our certifications. 
    1. Presentation → Set up the Confirmation Message that participants will see after filling out the form. Use the following template: `Thank you for completing the GitLab {Name} Knowledge Assessment! Please take this time to review your score. If you scored at least an 80%, you will receive your certificate via email. Please note this may take up to an hour to hit your inbox. If you did not score high enough to receive a certificate, you can take the quiz again. Please reach out to our team with any questions - learning@gitlab.com. Thanks! GitLab Learning & Development`
    1. Quizzes → toggling “on” the “Make this a quiz” button at the top. You can also uncheck “Point Values”
    1. Click “Save” when complete updating all three sections.
1. Go back through the questions and make them all required (including name & email) as well as set up the “Answer Key” along with assigning points per question.  
1. Quizzes have a pass % of >=80% to receive a certificate
1. Once the form is set up and ready, go to the responses tab and delete any responses that are in the form (i.e. any test responses that were submitted).
1. Open the quiz in the "View Form" format and complete the quiz (and pass it) - you will need at least 1 entry in the “passing_results” sheet to see the Sheet work (in Step 2) and get Zapier set up (in Step 4).

### Step 2: Google Sheets 

1. Once the form is set up, toggle to the “Responses” tab on the top of the form. Once there, select the green icon in the top right to “Create Spreadsheet” 
1. Once in the google sheet, the first sheet will be auto named “Form Responses 1” 
1. Create a second sheet and title it `passing_results` 
   1. In box A1 type `=QUERY(IMPORTRANGE("www.linktoresponsestabofspreadsheet.com", "Form Responses 1!A1:M10000"), "Select * WHERE Col9>=10")` 
      1. Where the link is for the sheet, update that to link to the Form Responses 1 Sheet 
      1. Where it says “Col9>=10, update the “9” with the number the column is with the scores (ex. A=1, B=2, C=3, etc.). Where the 10 is, update this number to the lowest score someone could get to score at least an 80% on the quiz. 

If someone will have to take more than one knowledge assessment to receive a certificate, the steps on how to set up the larger google sheet can be found in the video below: 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Vg4fG0vaCKY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line --> 

### Step 3: Google Slides

1. Once the Sheet is set up, you will need to get the certificate template form set up. Go to the "L&D" Folder in Google Drive and then into the sub-folder "Certifications/Knowledge Assessments" and the additional sub-folder "Certificates".
1. Make a copy of a current certificate by right clicking on the certificate ("GitLab Handbook First Documentation Certificate" is a good example of one to copy) and then clicking "Make a Copy". Replace the copied certificate title with the new certificate title. Click "OK". 
1. Open the slide to view the certificate. Update the title of the certificate on the slide. 

### Step 4: Zapier (Create New Zap)

1. [Log in to Zapier](https://zapier.com/platform/login) with the credentials in 1Password 
1. Go to the “Certifications/Knowledge Assessments” folder in Zapier  
1. Click “Make a Zap!” 
1. On the top left, click “Name your Zap” to name
1. 1 - When this happens…
   1. Select “Google Sheets” 
   1. Trigger Event = New Spreadsheet Row 
   1. Choose an Account = Your account
   1. Customize Spreadsheet Row 
   1. Spreadsheet = search for the name of the spreadsheet you created above 
   1. Worksheet = “passing_results” 
   1. Test and Continue 
1. 2 - Do This..
   1. Select “Google Slides” 
   1. Choose Action Event = Create Presentation from Template 
   1. Choose Account - “+ add new account” to connect your account 
   1. Title of New Presentation: GitLab {Title} Knowledge Assessment 
   1. Is Shared? = Yes 
   1. Template Presentation = search for the template 
   1. It will populate fields that need to be filled in the slides (i.e. name and date)
   1. Name: click the “insert field” button on the right, and search for the “First & Last Name” field on the spreadsheet 
   1. Date: click the “insert field” button on the right, and search for the “Timestamp” field on the spreadsheet 
   1. Click “Test and Continue” = this should make a test presentation in the same folder where the Google Slides Template is 
1. Select the “+” at the bottom of the current Zap 
   1. Select “Email by Zapier” 
   1. Select “Continue” 
   1. Customize Outbound Email: 
   1. 1. To: click the “insert field” button on the right, and search for the “email address” field under the new spreadsheet row option
   1. Subject: `Congratulations! Your GitLab {Title} Certificate is Inside!`
   1. Body: `Thank you so much for completing the {Title} Knowledge Assessment through GitLab. Your certificate is attached! Add it to your LinkedIn Profile or share it on Twitter. We'd love to see it! Please reach out to our team if you have any questions! GitLab Learning & Development`
   1. In the body, with your cursor on a blank link at the very bottom of the text box, you will also need to go to the “insert field button on the right. Select “Create Presentation from Template in Google Slides” - select “Export Links Application/pdf” 
   1. From Name: GitLab Learning & Development 
   1. Reply to: learning@gitlab.com
   1. Select Test & Review 
   1. Check your Email inbox for the test certificate 

### Step 4: Add to the Handbook 

1. There should be a handbook page with the content for the knowledge assessment. If there isn’t already, please create a handbook page first. 
1. At the bottom of the page, put the below template with information on how to access the knowledge assessment 
1. Here is an example:

```
## {Title} Knowledge Assessment

Anyone can test their knowledge on {insert cert name here}. To obtain a certificate, you will need to complete this [{name of quiz} quiz](insert link to quiz) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certificate that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at `learning@gitlab.com`.
``` 

## How to Add to the L&D Sisense Dashboard

### Step 1: Add to SheetLoad

1. Open the [SheetLoad Certificates](https://drive.google.com/drive/u/0/search?q=sheetload.values_certificate) sheet in Google Drive. If you do not have access, ask the Data Team for help in the #data Channel in Slack.
1. Create a new tab in the sheet that is the name of certificate in all lowercase, with no spaces and no punctuation, e.g. "values_certificate".
1. In cell A2 of the first sheet, add the formula to pull from the original responses. For example, `=IMPORTRANGE("https://link-to-responsessheet.com", "Form Responses 1!A2:M10000")`. Copy & paste column headers from the original sheet.
1. Make an MR to add the new tab name to the `sheets.yml` file under the [values_certificate name sheet](https://gitlab.com/gitlab-data/analytics/-/blob/master/extract/sheetload/sheets.yml#L111). [Example](https://gitlab.com/gitlab-data/analytics/-/merge_requests/2834/diffs#e66fb606d4d1df437ce4fe679e4431b6b90494c4_108_108)
1. Run the `clone_raw` and `sheetload` pipeline jobs. 
1. Assign the MR to the [Data Engineer supporting People](/handbook/business-ops/data-team/organization/#data-engineers) and cc the Data Analyst, People.

| Clone Raw Pipeline | Sheetload Pipeline |
| ------ | ------ |
| ![clone_raw_pipeline](/handbook/people-group/learning-and-development/certifications/clone_raw_pipeline.png) | ![sheetload_pipeline](/handbook/people-group/learning-and-development/certifications/sheetload_pipeline.png) |

**Important note:** The headers in the sheet MUST include `Timestamp`, `Score`, and `First & Last Name` exactly.

If you are updating this section, please refer to the [SheetLoad](/handbook/business-ops/data-team/platform/#using-sheetload) portion of the handbook and assign the MR to a member of the Data Team for review.

### Step 2: Add to dbt

This can be done in the same MR as Step 5 or as a separate MR. 

1. In the data team project, add a new file to the [sheetload base folder](https://gitlab.com/gitlab-data/analytics/-/tree/master/transform/snowflake-dbt/models/sheetload/base). The file name should begin with `sheetload`, then follow the same naming as the sheet, and end with a `.sql` file extension, such as `sheetload_compensation_certificate.sql`.
1. Declare each new tab as source in the `table` section of [sources.yml](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/sheetload/base/sources.yml)
1. The body of the file should be as follows
```
WITH source AS (

	SELECT *
	FROM {{ source('sheetload', 'compensation_certificate') }}

{{cleanup_certificates("'compensation_certificate'",
			"Email_address_(GitLab_team_members,_please_use_your_GitLab_email_address)")}}
```
where the line that begins with `FROM` is edited to reflect the name of the certificate, as is the line that begins with `{{cleanup_certificates`. The final line should include the email address column from the sheet exactly as is with spaces replaced as underscores. In other words if the column name is `"Email Address"`, it should be `"Email_Address"`. Remember to keep the quotes. 
1. Update the [sheetload_certificates_unioned](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/sheetload/xf/sheetload_certificates_unioned.sql#L9) model with a new row that follows the existing convention. If the new model's name is `sheetload_compensation_certificate.sql` the new row should be `ref('sheetload_communication_certificate'),`, including the comma. Avoid editing the last row in the list for ease. 
1. If this is a separate MR from Step 5, trigger the `specify_model` pipeline. If this is the same MR, trigger the `specify_raw_model` pipeline. At the next screen, on the Key include `DBT_MODELS` and the value is `+sheetload_certificates_unioned`.
1. Assign the MR to the Data Engineer support People and cc the Data Analyst, People.

| Specify Model Pipeline | Specify Raw Model Pipeline |
| ------ | ------ |
| ![specify_model](/handbook/people-group/learning-and-development/certifications/specify_model.png) | ![specify_raw_model](/handbook/people-group/learning-and-development/certifications/specify_raw_model.png) |
