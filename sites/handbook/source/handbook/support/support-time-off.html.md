---
layout: handbook-page-toc
title: Support Team Member Time Off
description: Guidelines for how time off applies to Support team members and what actions need to be taken.
---

## Support Team Member Time Off

The Support Team follows [GitLab's paid time off policy](/handbook/paid-time-off).
And we balance that with the need to deliver support to our customers every day.
This page is intended to provide all Support Team Members with an understanding
of what we need to do to achieve that balance, making it possible for all to
take time off as needed and desired while we as a team continue to deliver
amazing support and service to our customers.

There's a special point that needs to be emphasized, and that is that time off
for any team member only serves its purpose if that person truly takes the time
off. Nobody should feel pressure or the need to check Slack, email, or anything
else associated with work during their time off. **Especially when you are taking
time to be on holiday or to be with family or friends, please, in the timeless
words of the snow queen, Elsa, "let it go."**

## Responsibilities

When you are approaching a planned absence from work, it is your responsibility
to take reasonable actions to prepare the team and your customers for your
absence. By making these preparations, you will also be making it easier for
yourself to disconnect from work and make your time off effective.

### Choosing time off

A little thoughtfulness on everybody's part will go a long way toward making it
possible for everyone to take their desired days off. As you look to plan your
time off, please:

- ensure you know the [company wide paid time off guidelines](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)
- consider a different time off schedule if your regional capacity 
  calendar shows that on any of the days you would be absent the regional team
  would be short by 20% or more of its people
- schedule your time off as far in advance as you can, especially for days that
  may be popular or that are particularly important to you
- don't lock yourself into nonrefundable travel itineraries before you've taken
  steps such as the above to be sure you can get the planned days off
- if possible, plan your time off to avoid disruptions to the various rotations
  such as on-call

### One-time setup actions

1. Be sure you have access to the
[**Support - Time Off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com)
team calendar.

   You should have been granted access when you joined Support. But
if you don't have it, please ask for help in the `#support_team_chat` channel
on Slack and someone will share the calendar with you.

1. We recommend you set up the Google Calendar integration with PTO by Roots so that you do not need to populate your personal and "Support - Time Off" calendars manually.
   1. In Slack, click the `+` sign next to 'Apps' at the bottom of the left sidebar
   1. Search for 'PTO by Roots' and click 'View'
   1. Under 'Home', click on 'Your Events' to show a dropdown
   1. Click on 'Calendar Sync' under the Settings break
   1. Click on 'Connect your Calendar' and complete the actions to sync your calendar to PTO by Roots
      - You will see a 'Success! Your calendar has been connected.' message and
        your calendar listed under 'Your synced calendar' in PTO by Roots on Slack
      - ***NOTE: this also serves to block your availability in Calendly***
   1. After your personal calendar is linked, click 'Add calendar' under
   'Additional calendars to include?'. The 'Support - Time Off' calendar ID is
   `gitlab.com_as6a088eo3mrvbo57n5kddmgdg@group.calendar.google.com`

1. Verify the following calendars are shared with you. These are shared with the Support Team alias.
   1. [Support OOO - AMER](https://calendar.google.com/calendar/u/0/embed?src=c_rt0g4ptj1nut4045lcgsm21chg@group.calendar.google.com)
   1. [Support OOO - APAC](https://calendar.google.com/calendar/embed?src=c_408bhp8a8j579lqr99j552u21s%40group.calendar.google.com)
   1. [Support OOO - EMEA](https://calendar.google.com/calendar/u/0/embed?src=c_ev56c9rgjd9l1sn291i368jn78@group.calendar.google.com)

### Marking time off

Please take time off whenever you need it.

This page covers only considerations that are in addition to the company's [communicating time off guidelines](/handbook/paid-time-off/#communicating-your-time-off). So please familiarize yourself with those guidelines and in particular note the information regarding:

- rescheduling meetings
- using PTO by Roots

If you're taking off less than half of a day, you do not need to make entries in PTO by Roots or the Support Time Off Calendar, but please consider updating your Slack status.

Half day or more:

- Please enter the time off in PTO by Roots.
- If you didn't configure [Calendar integration with PTO by Roots](#one-time-setup-actions),
block off your own calendar and add full day or longer absences as a Free (not Busy) event to the Support Time Off calendar.

### Coverage for roles

If you are assigned to any PagerDuty shifts (such as on-call) during the time
   you are taking off, arrange for coverage:

1. Find a volunteer to cover for you; get help from your manager if necessary.
1. [Create the appropriate override in PagerDuty](https://support.pagerduty.com/docs/editing-schedules#create-and-delete-overrides).

### Coverage for assigned tickets

Groom your queue before going on PTO. As guidelines cannot cover every case, this section is meant to provide guidance for you to think critically about what is required for each ticket you're assigned to:

1. Is a customer waiting on you? Check with the customer.

   As per the [working with tickets workflow](/handbook/support/workflows/working-on-tickets.html#keep-in-mind),
   aim to update a customer daily. If your PTO will prevent a timely update, ask the customer whether they
   would prefer to pause the ticket till your return or have someone else step
   in to work with them. If they want to pause, put the ticket on-hold.
   Otherwise, find a new assignee.
1. For all tickets that won't be on hold, please write an internal note summarizing
   the status. Include all applicable parts from this list:

   - A concise statement of the problem or request
   - A brief explanation of what's been done so far - what's been tried, what
     progress has been made
   - The current status - what remains to be done
   - The plan - expected next steps and the reasons behind them

#### If your absence will be three or more business days

1. Notify the team of your upcoming absence

   Add an item in the regional (and .com) agenda the week before your absence to announce
   your time off.
1. Change your workflow in advance of your absence:
  1. A week in advance: Start thinking about whether you may need to hand off tickets, and pair with others to get them up to speed.
  1. Two to three days in advance: stop taking new tickets.
   1. work to resolve and close as many of your assigned tickets as possible
   1. contribute more to other people's tickets - internal comments, pairing
      sessions - to make up for not taking new tickets of your own
1. Find a new assignee for your tickets

   For all tickets, find a new assignee, and if possible review the tickets with them before
   reassigning.
   If you can't find someone to take one or more tickets, ask a manager to help.
   If you and the manager fail to find someone for any given ticket, as a last resort,
   you can 'unassign' yourself by assigning it to a 'group' ('Support EMEA', 'Support APAC', 'Support AMER').
   If the user has specified a preferred region, select the corresponding group.
   Otherwise, select any group - the ticket will still be visible for all Support Engineers.
   Ensure to follow the internal note process described above so that the next engineer can come up to speed quickly.
1. Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/index.html#set-your-current-status) to ensure you're not auto-assigned to GDPR requests:

   - The status text should contain one of the following strings: 'OOO', 'PTO', 'Parental Leave', or 'Friends and Family'
   - And the emoji should be one of the following: `:palm_tree:`, `:beach:`, `:beach_umbrella:`, `:beach_with_umbrella:`, `:ferris_wheel:`, `:thermometer:`, `:face_with_thermometer:`, `:red_circle:`, `:bulb:`, `:sun_with_face:`.

## How Support Handles Pandemic Support Days

During [Pandemic Support Days](/company/pandemic-support-day/#upcoming-pandemic-support-days) GitLab Support continues
to operate as normal. During the month of a Pandemic Support Days you should book off a day of your choosing to spend as you
please. Please follow the same guidelines as you would in [Choosing Time Off](#choosing-time-off).

## How Support Handles Holidays

During Holidays, GitLab Support continues to operate as normal. Please book holidays as required by your locality. For
non-statutory holidays, please follow the same guidelines as you would in [Choosing Time Off](#choosing-time-off).

As many customers may also take popular holidays Support Leadership will provide additional guidance in terms of 
the number of engineers required on those days.
 
## Tips

In addition to the companywide tips in
[Communicating Your Time Off](/handbook/paid-time-off/#communicating-your-time-off),
take a look at these tips to increase your ability to disconnect from work,
truly make the most of your time off, and make it easier to return to work:

1. Unsubscribe from any MRs that will likely resolve while you're away, and then
   you won't have to deal with all the emails from them when you return.
1. Set up some email filters to move emails into buckets that you can address
   one-at-a-time when you return.
1. Disconnect yourself from Slack and Gmail in any of these ways:
   *NOTE: PTO by Roots is supposed to turn off Slack notifications on your phone and
   set DND mode. But just in case, you may want to do one of these:*
   1. log out from Slack and Gmail on your phone, or
   1. remove Slack and Gmail from your phone, or
   1. pause the Slack and Gmail apps on your phone
   1. manually configure Slack and Gmail to send no notifications on your phone

## Re-connect

Please consider adding an item in the Support Week in Review after you get back to let the rest of the
team know what you've been up to!

