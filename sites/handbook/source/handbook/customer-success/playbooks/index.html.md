---
layout: handbook-page-toc
title: "Customer Success Playbooks"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---


Customer Success playbooks assist in selling, driving adoption, and ultimately delivering more value to customers via GitLab's DevOps platform. The below playbooks will include original content as well as links to other parts of the handbook.

## Structure of Playbooks

**Procedure**: Each Playbook contains the outline of steps to position, apply discovery questions, lead value discussions, and drive adoption (TAM only). The will be driven and tracked [via Gainsight](/handbook/customer-success/tam/gainsight/).

**Positioning**: Stage and use case value proposition and positioning with supporting collateral linked within Gainsight CTAs and Playbooks.

- Market requirements, personas, and differentiators
- Top features, stages, and categories
- Competitive assessments
- Customer slides
- Proof points, blogs, and customer success stories

**Discovery**: Discussion tools (e.g., discovery questions) using Command Plan approach.

**Adoption**: Adoption-related reference materials to accelerate adoption.

- Adoption map, noting recommend recommended features / use cases and sequence of adoption (where applicable)
- Product Intelligence attributes to track adoption
- Product documentation (Content owner: Product and Engineering Teams)
- Enablement and training assets
- Paid services

## Types of Playbooks

- **Risk**: Represents negative trends with customers. Examples include low license utilization, non-engaged customer and product risk.

- **Event/Lifecycle**: Lifecycle-based and typically scheduled events. Examples include onboarding, EBRs, and success planning.

- **Opportunity/Expansion**: Enablement/Expansion which is tied to revenue and/or product growth. Examples include stage adoption and uptiering.

- **Objectives**: Tied to Success Plans and can only be created in C360 > Success Plans section.

- **Activity**: Tied to 360 Timeline Activities. Activity is generally used as the default CTA type for CTAs created from the Timeline feature automatically. Users can also create CTAs of this type from the Cockpit page. Example includes account handoff AND update stale TAM sentiment.

- **Digital Journey**: Represents the Customer Digital Journey content. Examples include digital onboarding and adoption email series.

## Catalog of Playbooks

The following playbooks are aligned to our [customer adoption journey](/handbook/customer-success/vision/#high-level-visual-of-gitlab-adoption-journey) and support adoption of the related customer capability and [GitLab stage](/handbook/product/categories/). Within Gainsight, the TAM can manually add any Playbook ([instructions](/handbook/customer-success/tam/gainsight/#ctas)) to a Customer.

| Title | Live in GS? | Automated? | Gainsight Location | Type | Internal Reference Link |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Stage Adoption: Manage    | **Yes** | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Plan      | **Yes** | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Create    | **Yes** | Manual | Success Plan | Stage Adoption | [Source Code Management (SCM) / Create Stage](/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/) |
| Stage Adoption: Verify    | **Yes** | Manual | Success Plan | Stage Adoption | [Continuous Integration / Verify](/handbook/marketing/strategic-marketing/usecase-gtm/ci/) and [TAM CI Workshop](/handbook/customer-success/playbooks/ci-verify.html)|
| Stage Adoption: Package   | **Yes** | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Secure    | **Yes** | Manual | Success Plan | Stage Adoption | [DevSecOps / Security / Secure](/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/) |
| Stage Adoption: Release   | **Yes** | Manual | Success Plan | Stage Adoption | [Continuous Delivery / Release](/handbook/customer-success/playbooks/cd-release.html) |
| Stage Adoption: Configure | **Yes** | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Monitor   | **Yes** | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Protect    | **Yes** | Manual | Success Plan | Stage Adoption | |
| Selling Ultimate to New Customers   | **Yes** | Manual | Success Plan | ROI Success | |
| Ultimate uptier for existing customers  | **Yes** | Manual | Success Plan | ROI Success | |
| Server Migration - On-Prem to On-Prem    |**Yes** | Manual | Success Plan | Stage Adoption | |
| Server Migration - On-Prem to Self-Managed Cloud    |**Yes** | Manual | Success Plan | Stage Adoption | |
| Server Migration - On-Prem/Cloud to K8S    |**Yes** | Manual | Success Plan | Stage Adoption | |
| Server Migration - Self-Managed to GitLab SaaS    |**Yes** | Manual | Success Plan | Stage Adoption | |
| Geo Implementation  |**Yes** | Manual | Success Plan | ROI Success | |
| Account Triage | **Yes** | Automated | CTA | Risk |      |
| Low License Utilization | **Yes** | Automated | CTA | Risk |      |
| Product Risk | **Yes** | Manual | CTA | Risk |      |
| Non-Engaged Customer | **Yes** | Manual | CTA | Risk |      |
| CI Health Enablement/Expansion Review | **Yes | Automated | CTA | Activity | 
| Create Success Plan | **Yes** | Automated | CTA | Lifecycle |      |
| Customer Offboarding | **Yes** | Automated | CTA | Lifecycle |      |
| Executive Business Reviews | **Yes** | Automated | CTA | Lifecycle | [EBR in a Box](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)     |
| New Customer Onboarding | **Yes** | Automated | CTA | Lifecycle |      |
| High License Utilization: Growth Opportunity | **Yes** | Automated | CTA | Lifecycle |      |
| Usage Ping Enablement | **Yes** | Manual | CTA | Lifecycle | [Usage Ping FAQ](/handbook/customer-success/tam/usage-ping-faq/) |
| Onboarding Pilot | **Yes** | Automated | CTA | Lifecycle |      | 
Upcoming Renewal | **Yes** | Automated | CTA | Renewal |      |
| Prometheus & Grafana | **Yes** | Manual | Not Available | Not Available |  [Internal link - existing Playbook](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)    |
| GitLab Days | **Yes** | Manual | Not Available | Not Available | [Internal link - existing Playbook](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)     |
| Account Handoff | **Yes** | Manual | CTA | Activity |      |
| Update Stale TAM Sentiment | **Yes** | Manual | CTA | Activity |      |
| New TAM Account Assignment | **Yes** | Automated | CTA | N/A |  |
| [GitLab Server Migrations](/handbook/customer-success/playbooks/server-migrations.html) | **Yes** | Manual | CTA | Not Available | [Internal link - existing Playbook](https://docs.google.com/spreadsheets/d/1cP6czE6zZ9EWT5HGOF2MGP2repiV0GI8a8V2i9iK9vM/edit#gid=0)  |
| Digital Onboarding - Welcome Email | **Yes** | Manual | CTA | Digital Journey |      | 
| Digital Onboarding - Self-Managed Alternative Backup Strategies | **Yes** | Manual | CTA | Digital Journey |      | 
| New Customer Digital Onboarding - SaaS Email Series | **Yes** | Manual | CTA | Digital Journey |  [Internal link - existing Playbook](https://about.gitlab.com/handbook/customer-success/tam/digital-journey/)     |
| New Customer Digital Onboarding - Self-Managed Email Series | **Yes** | Manual | CTA | Digital Journey |  [Internal link - existing Playbook](https://about.gitlab.com/handbook/customer-success/tam/digital-journey/)     |
| Digital Onboarding - CI Enablment (Verify) | **Yes** | Manual | CTA | Digital Journey | [Internal link - existing Playbook](https://about.gitlab.com/handbook/customer-success/tam/digital-journey/)     | 


TAMs create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab. We currently have a [Stage Adoption Guideline](/handbook/customer-success/tam/stage-adoption/) to assist with understanding where a customer stands, and we are working on merging adoption maps into the use case pages.
