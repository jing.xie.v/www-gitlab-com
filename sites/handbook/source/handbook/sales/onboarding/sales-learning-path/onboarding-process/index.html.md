---
layout: handbook-page-toc
title: "Sales & Customer Success Onboarding Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sales Onboarding Process
1.  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
    - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md)
1.  In the "Day 2" section of the general GitLab onboarding issue, a role-based entitlement access request (AR) will be created automatically for new team members if a [pre-approved template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) exists. The new team member will be tagged in the AR automatically, and the AR will also be automatically added to the new team member epic together with their onboarding issue. Pre-approved role-based entitlement access request templates for field roles are available here:
    - [department_business_development](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_business_development)
    - [department_channel](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_channel)
    - [department_commercial_sales](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_commercial_sales)
    - [department_customer_success](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_customer_success)
    - [department_enterprise_sales](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_enterprise_sales)
    - [department_field_operations](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_field_operations)
1.  In the "Sales Division" section of that issue, Field Enablement is tagged with the action to add the new sales team member to the Sales Quick Start learning path in Google Classroom according to the SQS Workshop they plan to attend. They will be added to the learning path within their first week of joining GitLab. This learning path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.
1.  The new sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quick Start learning path  
    - When possible, Sales Enablement will also update the new team member's onboarding issue and/or add a comment to explicitly reference the link to their Sales Quick Start learning path
    - In the "New Team Member" section, there is a specific action for the new hire to "Complete your Sales Quick Start learning path"
    - See what's included in the virtual, self-paced [Sales Quick Start learning path in Google Classroom](/handbook/sales/onboarding/sales-learning-path/)
    - Non-Sales team members can choose to add themselves to the SQS Google Classroom Master Learning Path regardless of role if interested in understanding what new Sales Team Members are expected to complete prior to attending an SQS Workshop (Sales team members will be added to their appropriate cohort’s Google Classroom Learning Path by Sales Enablement)
        -  [SQS Google Classroom Master Learning Path](https://classroom.google.com/c/NjIxMTgzNzcyMzda)
        -  Code: 4fege7p
