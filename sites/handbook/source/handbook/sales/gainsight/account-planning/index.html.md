---
layout: handbook-page-toc
title: "Using Gainsight within Sales"
description: "The key aspects of how Sales uses Gainsight to drive success for customers."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

*For an overview of Gainsight, and information about how to login, please refer to the [Gainsight Overview Page](/handbook/sales/gainsight/).*


## Gainsight workflow

Gainsight holds a lot of information and is great for reporting, but there can be a learning curve for SALs to getting a good workflow where they feel productive. Below is a recommended workflow for using Gainsight.

...

### Gainsight Dashboard

When logging in, SALs will begin on their Customer 360 Dashboard. Gainsight's homepage will show the Top 20 SaaS Customer by ARR. On this page, you can review the customer's Lifecycle Stage, Known License Utlization, Health Score, your Open Calls to Action (CTAs), Open Tickets, and the last Activity Date. 

This dashboard is likely to be a SALs most-frequented page, as it shows a summary of their entire book of business. 

### Account Profile

TBD

### Relationship and Influence Mapping

TBD

### Whitespace Mapping

TBD

### Action Plan

TBD

### Video Enablement 

Coming Soon! 
