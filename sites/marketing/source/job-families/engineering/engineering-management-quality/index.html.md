---
layout: job_family_page
title: "Engineering Management - Quality"
description: "Engineering Managers in the Quality department are motivated and experienced leaders who grow our test automation efforts across the entire GitLab ecosystem."
---

# Quality Engineering Management Roles at GitLab

At GitLab Quality is every Engineer's responsibility and the output of the Quality department is instrumental to GitLab’s success.


Engineering Managers in the Quality department are motivated and experienced leaders who grow our test automation efforts across the entire GitLab ecosystem.
They demonstrate a passion for high quality software, strong Engineering principles and methodical problem solving skills.

They own the delivery of test automation tools and strategy and are always looking to improve Engineering productivity. They also coordinate across departments and teams to accomplish collaborative goals.

Quality Engineering at GitLab spans both Software Engineers in Test and Engineering Productivity.
Engineering Productivity is a new branch in career evolution of Quality Engineering.
* https://testing.googleblog.com/2016/03/from-qa-to-engineering-productivity.html
* https://saucelabs.com/blog/qa-is-not-enough-you-need-to-engineer-productivity

## Manager, Quality Engineering

### Job Grade 

The Manager, Quality Engineering is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Help define the team's road map and author project plans to deliver against that roadmap.
* Draft team's quarterly OKR and manage the team's priorities.
* Drive improvements to test framework architecture and test coverage.
* Work across engineering to inject testing earlier into the software development process.
* Drive adoption of best practices in code health, testing, testability and maintainability (clean code, test pyramid).
* Track test gaps, quality, and productivity metrics. Work with other engineering teams to improve gaps from this data.
* Recommend improvements into overall best practices, design, testability and quality and productivity.
* Recruit Engineers, grow the team and stay in step with the hiring plan.
* Help engineers grow their skills and experience.
* Hold regular 1:1's with all members of their team.
* Create a sense of psychological safety on their team.
* Excellent written and verbal communication skills.
* Give clear, timely, and actionable feedback.
* Strong sense of ownership, urgency, and drive.


### Requirements
* 8+ years of experience in the Quality Engineering/Engineering Productivity field.
* 2+ years of experience managing a team of Software Engineers in Test or Engineering Productivity Engineers.
* Strong experience developing in Ruby
* Strong experience using Git
* Strong experience with UI & API test automation tools, particularly in Ruby stack (Capybara, Watir, Selenium and etc.).
* Experience working with Docker containers
* Experience with AWS or Kubernetes
* Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)
* Experience defining high-level test automation strategy based on DevOps industry's best practices.
* Experience driving organizational change with cross-functional stakeholders.
* Enterprise software company experience.
* Computer science education or equivalent experience.
* Passionate about open source, developer tools, and shipping high-quality software.
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/values/), and work in accordance with those values.
 * [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
* Ability to use GitLab


### Interview Process
Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
As a result an interview can be canceled at any time even if the interviews are very close (e.g. a few hours apart).

* A short questionnaire from our Recruiting team
* 30 minute [Screening call](/handbook/hiring/#screening-call) with a recruiter
* 1 hour interview with the Director of Quality (manager)
* 1 hour technical Interview with a Senior or Staff Engineer from the Quality Engineering department.
* 1 hour interview with a Senior or Staff Engineer (report)
* 1 hour interview with Manager, Quality Engineering (peer)
* 45 minute interview with VP of Engineering

## Director, Quality Engineering

The Director, Quality Engineering role extends the [Manager, Quality Engineering](#quality-engineering-manager) role.

### Job Grade 

The Director, Quality Engineering is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Define and own all of GitLab's Quality Engineering efforts.
* Drive the Quality Engineering sub-department's roadmap and quarterly OKRs.
* Hire world class Engineering Managers for the Quality Engineering sub-department.
* Manage multiple Quality Engineering teams and projects.
* Track, publish and socialize the progress of the department.
* Drive improvements and process that impacts Quality.
* Manage the sub-department’s priorities, set guidelines for Quality Engineering teams and align them strategically across all of Engineering.
* Excellent written and verbal communication skills. Able to convey bottom-line messages to executives at C & VP level.
* Hold regular skip-level 1:1's with all members of their team.
* Help the teams in Quality Engineering grow their skills and experience, ensure that they are happy and productive.
* Interface with GitLab customers to address Quality and Performance gaps.
* Research and advice team on new technology and tools in the Quality Engineering space.
* Represent the company publicly at conferences.

### Requirements

* This position does not require extensive Ruby development experience but the candidate should be very familiar with
industry standard libraries (test tooling, orchestration, performance, and etc) and able to provide feedback on these topics.
* Experience leading Quality focused SLA driven metrics across the Engineering organization (time to resolve bugs, time to merge code, and etc).
* Experience presenting at conferences and meet-ups in the Quality Engineering space.
* Proven track record of shipping multiple iterations of an Enterprise Product.
* You share our [values](/handbook/values/), and work in accordance with those values.

## VP of Quality

The VP of Quality Engineering role extends the [Director, Quality Engineering](#director-quality-engineering) role.

### Job Grade 

The Director, Quality Engineering is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Define and own all of GitLab's Quality Engineering, Engineering Productivity, Engineering Analytics, and Engineering Open Source efforts.
* Drive the Quality Department's roadmap and quarterly OKRs.
* Set an ambitious vision for the department, product, and company.
* Manage the department's budget.
* Hire world class leaders for the Quality department.
* Ensure that the department recruiting efforts stay in step with the product roadmap and company hiring plan
* Track, publish and socialize the progress of the department.
* Drive improvements and process that impacts Quality.
* Manage the department’s priorities, set guidelines for Quality sub-departments and teams and align them strategically across all of Engineering.
* Work across the company to define and implement mechanisms to bake in Quality and testing earlier in the software development process.
* Excellent written and verbal communication skills. Able to convey bottom-line messages to executives at C & VP level.
* Hold regular skip-level 1:1's with all members of their team.
* Help the teams in the Quality Department grow their skills and experience, ensure that they are happy and productive.
* Interface with GitLab customers to address Quality and Performance gaps.
* Research and advice team on new technology and tools in the Quality space.
* Represent the company publicly at conferences.

### Requirements

* This position does not require extensive Ruby development experience but the candidate should be very familiar with
industry standard libraries (test tooling, orchestration, performance, and etc) and able to provide feedback on these topics.
* Experience driving the adoption of the DevOps across the Engineering organization (test pyramid, shifting left, and etc).
* Experience leading Quality focused SLA driven metrics across the Engineering organization (time to resolve bugs, time to merge code, and etc).
* Experience presenting at conferences and meet-ups in the Quality Engineering space.
* Proven track record of shipping multiple iterations of an Enterprise Product.
* You share our [values](/handbook/values/), and work in accordance with those values.

## Performance Indicators

Quality Engineering Management has the following job-family performance indicators.

* [Hiring actual vs plan](/handbook/engineering/quality/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/quality/performance-indicators/#diversity)
* [Handbook update frequency](/handbook/engineering/quality/performance-indicators/#handbook-update-frequency)
* [Team member retention](/handbook/engineering/quality/performance-indicators/#team-member-retention)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
