- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: 1.3
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - We seem to have had a performance regression, which is showing up in our page load times. This is corroborated by our Sitespeed testing, with 13% fewer pages <2.5s and average LCP growing by 0.5s. We [need to investigate](https://gitlab.com/gitlab-org/gitlab/-/issues/326154). This does not seem related to our stability challenges recently.
  implementation:
    status: Complete
    reasons:
      - We are still working to [improve our per-geo views](https://gitlab.com/gitlab-data/analytics/-/issues/8014).
      - Established a target of 1.3s for median page load time. The p90 and p95 consistently track the trends in median, and so we should be able to just pick one. Rationale for 1.3s is that we attained that once back in Jan/Feb, however have regressed since. We should attempt to reverse the trend we've seen in the last few months.
  lessons:
    learned:
    - We did not have a front-end performance OKR this past quarter, and saw regressions on both sitespeed LCP as well as RUM page load timings. This seems to suggest that we need to improve our tooling and awareness of these metrics so we no longer need OKR's to maintain desired performance levels.
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
  metric_name: performanceTiming
  sisense_data:
    chart: 10546836
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - Age seems to have leveled off, % on latest 3 versions begins to climb after low January numbers.
    - Positive month across the board, need to analyze possible reasons to keep momentum going.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - This month appears to have steered away from the expected result slightly and the trend potentially [is beginning to level off](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380) for median age of version for all versions and license plans. We do not know what has caused this change in trend. This [will be investigated](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/821) to see how we can use these results to beginning a downward trend. 
    - There was a slight upward trend for all install types for [percentage on the latest 3 versions.](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8890933&udv=1059380) The helm install is still well above the target of 40, and the other install types are beginning to approach 40. Perhaps this bottom of the dip was due to low upgrade rate around/after the holiday season in January. A study was also conducted on [ease of upgrading](https://gitlab.com/gitlab-org/ux-research/-/issues/1114) for users. This in conjunction with analysis of causes for the trend will be used for actionable issues to drive positive metrics directions.
    - We are in the process of analyzing the feedback from a study related to [the ease of install and upgrade](https://gitlab.com/gitlab-org/ux-research/-/issues/1301) now. We have also recieved some results from this [study](https://gitlab.com/gitlab-org/ux-research/-/issues/1250#note_526369784), which was focused on the choice of CE vs EE and respondants left some comments related to openess to receive more notifications about upgrading. We will conduct more research on the best way to notify users of upgrading and how to automate the upgrade process. The initial issue can be found [here](https://gitlab.com/gitlab-org/ux-research/-/issues/1377)
    - Work has begun on [better highlighting](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators. This will impact results once it has been completed, estimated for next milestone.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) to the usage data. These metrics are not yet available; we are working on getting them plotted in Sisense. We also intend to add tracking for [git push operations on a Geo secondary](https://gitlab.com/gitlab-org/gitlab/-/issues/320984).
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we will begin tracking Git pulls in 13.11 with tracking for Git pushes planned as part of the [Geo Usage Ping Epic](https://gitlab.com/groups/gitlab-org/-/epics/4660). We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. We've recently completed a PoC to make the secondary UI indistinguishable from the primary. Implementation work is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster, but if one occurs everyone benefits. Based on [the node number distribution](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=6471733&udv=0) ~60% of our customers use Geo mainly for DR. I think this is worth measuring because setting up Geo is always a conscious decision by the customer - it must be configured.
    - We are working to add support for replicating all data types, so that Geo is on a solid foundation for both DR and Geo Replication.The Testing group added [replication for Pipeline Artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/238464) in 13.11.
    - In April 2020 (before the release of 12.10), we replicated ~56% of all data types (13 out of 23 in total) and verified ~22%. In 13.11 we replicate ~86% of all data types (25 out of 29 in total) and verify ~45%. In the last year GitLab released six new features that needed Geo support. We replicate a 100% of those new features and verify ~57%
 
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - While there was a large jump from January to February, the number is still very low right now. Two potential reasons a) data is incomplete b) WebUI is read-only and requires a different UI. We are planning to change this, see [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing) and in-progress implementation work in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    - Steady month over month growth with [4.6% increase in new Geo customers from March to April](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=9939914&udv=0).
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2    

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
    - A bug in the plot displays the versions out of order (13.11 next to 13.1). Investigating a fix.
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption has stabilized in versions 13.7-13.10 but increased slightly in 13.11.
    - We are currently working on reducing [the memory consumption for Puma and Sidekiq](https://gitlab.com/groups/gitlab-org/-/epics/5622).
    - To further reduce memory consumption, we are planning to work on [splitting the application into functional parts to ensure that only needed code is loaded](https://gitlab.com/groups/gitlab-org/-/epics/5278) and on [reducing the memory consumption for Puma and Sidekiq endpoints](https://gitlab.com/groups/gitlab-org/-/epics/5622).

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - In March, Paid GMAU is up 6.2%.  SaaS up 5.4%, Self-Manged up 11.6% 
    - The Self-Manged growth is due to the increased interest in code search of existing very larger customers
  implementation:
    status: Complete
    reasons:
    - Data collected is incomplete for September and October 2020 and January 2021
  lessons:
   learned:
    - The performance of the SaaS Latency has improved by 50%
    - There is an increase in interest in code search across organizations. In the last 6 months, we have seen the request for “product to customer engagement” grow by 700% 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - The query Apdex has decreased in all charts after the release of 13.10, including [the weekly one](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=9885649&udv=0), which has a pretty constant number of instances reporting stats. No similar trend on the GitLab.com charts, nor any other clear signal indicating what may be affecting the Apdex. We'll monitor closely and will revisit this in ~2 weeks.
    - We expect the updates performed in the scope of [Research Database queries for performance and frequency](https://gitlab.com/groups/gitlab-org/-/epics/5652) to further improve the ratio of queries which complete within 250ms and lower the variance of the Apdex even further.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/yxe4pv4a) and [50ms - Tolerable 100ms](https://tinyurl.com/y6latcuc)) but reflects with sharp drops the production incidents that related to the database.
    - The vast majority of GitLab 13.9 instances use PostgreSQL 12.5. Only 2.4% of EE instances are still in PostgreSQL 11.x.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 122000
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Paid Monthly Active Users on GitLab.com increased by 5.9% in March to 120K.
    - Reset Q1 target after surpassing original target last month. Will set a Q2 target in next month's update.
    - Insight - The increase was driven by several large [expansion deals in the Premium tier](https://app.periscopedata.com/app/gitlab/710777/Infra-PM-Dashboard?widget=9848967&udv=1113414).
    - Improvement - To influence Paid GMAU, we have created a [framework](https://about.gitlab.com/handbook/product/product-processes/#saas-first-framework) to guide SaaS-first thinking among PMs. Additionally, we are focusing on improving uptime and stability of GitLab.com via the InfraDev process, introducing error budgets with stage groups, and giving them [metrics dashboards](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/406) to better monitor their product areas. Additionally, we are tracking key features needed to achieve [Enterprise Readiness on GitLab.com](https://gitlab.com/groups/gitlab-org/-/boards/2037713) and extending our [compliance posture](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/2505) on Gitlab.com.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Need to account for seasonal fluctuations into target projections. 
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2
