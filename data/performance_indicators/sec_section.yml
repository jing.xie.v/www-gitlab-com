- name: Sec - Section CMAU - Reported Sum of Sec Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across all stages in the Sec Section (Secure, Protect) for All users and Paid users.
  target: 
  org: Sec Section
  section: Sec
  public: true
  pi_type: Section CMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982146
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10224291
    dashboard: 758607
    embed: v2
- name: Sec, Secure - Section MAU, SMAU - Unique users who have used a Secure scanner 
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: sec
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982579
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9984584
    dashboard: 758607
    embed: v2
- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Known [data extraction issue](https://gitlab.com/gitlab-data/analytics/-/issues/8218) preventing SaaS usage data from being displayed. We're currently flying blind until this is fixed. This affects all Secure metrics. 
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - All metrics are derived from data team's data mart and thus any issues with such will create a cascade effect [across all my group dashboards](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin). I hadn't realized the extent of this risk prior to the current data issue. 
  monthly_focus:
    goals:
      - Now that SAST & Secret Detection is available to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [Improved MR experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) is now complete & [Improved Configuration Experience](https://gitlab.com/groups/gitlab-org/-/epics/4787) is now available to .com customers. Both of these have been slowed by complexities with licensing specific features and differences between code structure in EE and CE. However, this should help us maintain our growth rate from SAST to Core and create an upgrade path. We have chosen to [pause additional work to expand these features to CE users](https://gitlab.com/groups/gitlab-org/-/epics/5460) given the licensing complexities. 
      - Moving forward we are focused strongly on data accuracy and reduction of false positives. We have a [new exploration to validate a new vulnerability tracking improvement from Vulnerability Research](https://gitlab.com/groups/gitlab-org/-/epics/5144) which could materially reduce false positives. This approach is appearing to work out and produce more accurate tracking. This also opens a new metrics problem that we need a way to track accuracy with real-world data. We intend to measure the improvements of this change via the [new vulnerabilities data](https://app.periscopedata.com/app/gitlab/801442/Taylor's-Vuln-Exploration) in Sisence to ensure this change results in material positive impact before rolling it out.
      - To increase efficiency and reduce maintenance costs of our 10 linter analyzers we are [transitioning to SemGrep for many of our linter analyzers](https://gitlab.com/groups/gitlab-org/-/epics/5245). If successful we will reduce 10 open source analyzers into 1 while also improving the detection rules and proving a more consistent rule management experience across these analyzers, this will be materially impactful as we seek to build our proprietary SAST engine on top of these results. This effort is on track and likely to see our first SemGrep analyzer released by 14.0. 
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9980130
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9967764
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, February showing higher usage than January.
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - There is a bump in February's usage numbers, despite there being no major features released to cause it. The bump is likely due to organic growth and the normal DAST testing cycle.
      - Customers have stopped using DAST in the past because of the huge number of vulnerabilities produced in the dashboard, vulnerability aggregation is the first step in addressing this.
      - More profile options (specifically authentication) need to be introduced for on-demand DAST scans to have broad usage or to be considered GA.
  monthly_focus:
    goals:
      - Aggregating noisy vulnerabilities to stop DAST from overwhelming the Vulnerability Report (https://gitlab.com/gitlab-org/gitlab/-/issues/254043)
      - Add more options to Site profile (https://gitlab.com/groups/gitlab-org/-/epics/3771)
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9980137
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9980139
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or number of unique users who have run one or more License Scanning jobs.
  target: 2% higher than current month (SaaS and self-managed combined) for All and Paid
  org: Sec Section
  section: sec
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Currently our data is MVC and a lagging indicator, trends are accurate but due to extrapolation of self-hosted, low opt-in of self-hosted, overall missing granularity, and 28 day cycles actual numbers are imprecise, in addition I believe there was another data event this month resulting in not being able to view SaaS, but using my own data charts as well as this charts self hosted data, it allows high level trending comparisons.
  lessons:
    learned:
      - Nothing through use of xMAU Metrics.
  monthly_focus:
    goals:
      - Handle the rapid action event and then focus on [Software Composition Analysis removals and deprecations for 14.0](https://about.gitlab.com/blog/2021/02/08/composition-analysis-14-deprecations-and-removals/). We hope there is then time for [Consistency in default behaviour of AST scanners and jobs](https://gitlab.com/groups/gitlab-org/-/epics/5334). After which we will resume to our usual, nothing directly related to these lagging indicators, but continue work on [Dependency Scanning to complete](https://gitlab.com/groups/gitlab-org/-/epics/1664) however [auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/3188) is still blocked by a token issue.
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9967897
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10102606
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - We have a good growth rate and need more users to use fuzz testing.
  implementation: 
    status: Definition
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Data [integrity issues](https://gitlab.com/gitlab-data/analytics/-/issues/8218) with processing usage ping means we aren't confident in this month's data or its relationship to previous trends.
  monthly_focus:
    goals:
      - Corpus management for coverage-guided fuzz testing (https://gitlab.com/groups/gitlab-org/-/epics/2915)
      - Add support for AFL fuzz targets (https://gitlab.com/gitlab-org/gitlab/-/issues/321075)
      - Improve our documentation based on known pain points (https://gitlab.com/gitlab-org/ux-research/-/issues/1273)
      - Continue analysis of new data to understand how often and how many users successfully use fuzz testing.
      - Monitor inflow of new data and update performance indicator charts as needed.
      - Any changes needed to restore or update reporting as a result of the outages.
  metric_name: user_coverage_fuzzing_jobs, user_api_fuzzing_jobs, user_api_fuzzing_dnd_jobs on self-managed, (options like '%gitlab-cov-fuzz%' OR SECURE_CI_JOB_TYPE='api_fuzzing') on .com
  sisense_data:
    chart: 9842394
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >2% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - GMAU remains high, despite [March data issue](https://gitlab.com/gitlab-data/analytics/-/issues/8218)
  implementation:
    status: Definition
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Project-level and Pipeline features significantly increased Feb & March
      - Group-level growth flattened after increase through January
      - Pipeline security views showing increase beyond MR referrals, could indicate users discovering a new area finding value
      - This could indicate cyclic usage patterns or new teams exploring optimal workflows
  monthly_focus:
    goals:
      - Continue executing on roadmap based on input from other sensing mechanisms
      - Evaluate shifting focus to more Group-level features based on usage patterns
      - Gather input on [future design vision](https://gitlab.com/groups/gitlab-org/-/epics/4812) via [Kano analysis and WSJF](https://gitlab.com/gitlab-org/ux-research/-/issues/1295)
      - Complete Dismissal [types / reasons](https://gitlab.com/gitlab-org/gitlab/-/issues/196976)
      - Complete Bulk [updates](https://gitlab.com/groups/gitlab-org/-/epics/4649)
      - Begin steps 2-4 of MR security widget [UX improvement](https://gitlab.com/groups/gitlab-org/-/epics/4427)
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of users who have run a container scanning job or interacted with the Protect UI in the last 28 days of the month.
  target: Progressively increasing month-over-month, >2%
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Usage increased significantly from January -> February.  This was unexpected and was not likely caused by product changes.
    - Usage growth is projected to continue to decline in the short-term.  See lessons learned section for more details.
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
    - Although we have released the Alert Dashboard, we do not yet have a good way of tracking monthly active users for this area. Instrumentation is expected to be complex (1-2 months of one developer's time) and usage is expected to remain low until additional alert times are added to the dashboard. Currently instrumenting these metrics is a low priority.
    - The Alert Dashboard is fully available for self-managed.  It has also been instrumented for SaaS; however, customers are only able to use it on a per-request basis until GitLab KAS is fully rolled out to SaaS.
  lessons:
    learned:
      - As Container Security has not been resourced for some time, we have a pent-up list of feature requests and on-going maintenance work that is presenting a strong headwind to increasing usage.
      - Usage growth is anticipated to be minimal or negative until we can make a dent in the outstanding usability challenges.  Work is anticipated to be completed by March, with usage numbers recovering across the March-June timeframe.
      - The release of the Security Orchestration category (planned for February) will also bolster the Group's usage numbers once we instrument metrics tracking for the category.
  monthly_focus:
    goals:
      - Project-level DAST Scan Schedule Policies (pipeline) (https://gitlab.com/groups/gitlab-org/-/epics/5329) is mostly available behind a feature flag
      - Project-level DAST Scan Schedule Policies (scheduled scans) (https://gitlab.com/groups/gitlab-org/-/epics/5360) is being worked on for an April release
      - Replacing Clair with Trivy (https://gitlab.com/groups/gitlab-org/-/epics/5398) will set the foundation for us to be able to scan containers in production environments
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 10039269
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039569
    dashboard: 758607
    embed: v2
- name: Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >10% month-over-month
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: false
  is_key: false
  health:
    level: 2
    reasons:
    - Growth from January -> February has slightly decreased
    - This is not unexpected as we have significantly reduced our investment in this area
  implementation:
    status: Complete
    reasons:
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category is relatively new
  lessons:
    learned:
      - We have 22 clusters using CNS, 21 of which are on gitlab.com and 1 is self managed.  Other self-managed customers may be using CNS but not reporting usage ping.
      - Customers are using the blocking mode as a significant amount of traffic is being dropped.
      - With the Defend -> Protect changes, we have significantly reduced our investment in this area
  monthly_focus:
    goals:
      - We are temporarily not investing in this area so we can allocate more resources to Security Orchestration and Container Security
  metric_name: clusters_applications_cilium
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2
