title: Continuous delivery
seo_title: Continuous delivery (CD)
description: Learn how continuous delivery automates the application release process
header_body: >-
  Continuous delivery automates the application release process so that
  deployments are predictable and repeatable.



  [Watch the "Keys to accelerating software delivery" webcast →](https://about.gitlab.com/webcast/justcommit-reduce-cycle-time/)
canonical_path: /topics/continuous-delivery/
file_name: continuous-delivery
twitter_image: /images/opengraph/ci-cd-opengraph.png
related_content:
  - title: How continuous integration and continuous delivery work together
    url: /topics/ci-cd/continuous-integration-continuous-delivery-work-together
cover_image: /images/topics/g_gitlab-ci-cd.svg
body: >+
  Continuous delivery (CD) is a software development practice that works in
  conjunction with continuous integration to automate the application release
  process. Once continuous integration builds and tests code in a shared
  repository, continuous delivery takes over during the final stages to ensure
  software releases are low-risk, consistent, and repeatable. With continuous
  delivery, software is built so that it can be deployed to production at any
  time. 


  [Continuous delivery](/stages-devops-lifecycle/continuous-delivery/) is often used interchangeably with continuous deployment, but there is a subtle difference between the two. Continuous deployment means that all CI-validated code deploys to production *automatically*, whereas continuous delivery means that this code *can* be deployed. The flexibility for code to be deployed at any time is what differentiates delivery from deployment, and practicing continuous deployment is possible when continuous delivery is already in place.


  > **[Continuous integration](/topics/ci-cd/)** is the practice of integrating code into a shared repository and building/testing each change automatically, as early as possible; usually several times a day.


  > **Continuous delivery** ensures CI-validated code can be released to production at any time.



benefits_title: What are the benefits of continuous delivery?
benefits_description: "\n"
benefits:
  - title: Releases are low-risk
    description: The ultimate goal for software deployments are to make them as
      boring as possible. A low-risk event, by its very nature, will be painless
      and boring. That sounds like a perfect deployment to us.
    image: /images/icons/gitlab-pipeline.svg
  - title: Deploy more frequently
    description: If deployments are boring and low-risk, teams will do them more often.
    image: /images/icons/header-double-arrow-right.svg
  - title: Remove deployment bottlenecks
    description: No more barriers – continuous delivery keeps the pipeline going all
      the way to end users.
    image: /images/icons/gitlab-rocket.svg
  - title: Roll out and rollback on demand
    description: "Deploy with confidence "
    image: /images/icons/mvp.png
resources_title: Continuous delivery resources
resources_intro: >-
  Use these resources to learn more about continuous delivery. We would love to
  get your recommendations on books, blogs, videos, podcasts and other resources
  that

  tell a great CD story or offer valuable insight on the definition or

  implementation of the practice.


  Please share your favorites with us by tweeting us [@gitlab](https://twitter.com/gitlab)!
resources:
  - title: Voted as a Strong Performer in The Forrester Wave™ Continuous Delivery
      And Release Automation, Q2 2020
    url: https://about.gitlab.com/analysts/forrester-cdra20/
    type: Reports
  - title: Hemmersbach reorganized their build chain and increased build speed 59x
    url: /customers/hemmersbach/
    type: Case studies
suggested_content:
  - url: https://about.gitlab.com/blog/2019/08/06/feature-flags-continuous-delivery/
  - url: https://about.gitlab.com/blog/2020/07/23/safe-deploys/
  - url: https://about.gitlab.com/blog/2019/04/19/progressive-delivery-using-review-apps/
  - url: https://about.gitlab.com/blog/2019/10/30/secure-journey-continuous-delivery/
